package com.inapp.nlp.process.test;

import static org.hamcrest.CoreMatchers.hasItems;

import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.Collection;
import org.junit.Test;

import com.inapp.nlp.process.NLPSequenceSearcher;

public class NLPSequenceSearcherTest {

	NLPSequenceSearcher classUnderTest = null;

	String modelFile = "./src/test/resources/test.model";

	@Test
	public void testFindNearWordsAll() {

		NLPSequenceSearcher thisInsAll = null;

		File f = new File(modelFile);
		if (f.exists() && !f.isDirectory()) {
			try {
				thisInsAll = NLPSequenceSearcher.getInstance(modelFile);
			} catch (Exception e2) {
				System.out.println("Error....." + e2.getLocalizedMessage());
			}
			String expected1 = "donald";
			String expected2 = "president";
			String expected3 = "cohen";
			try {
				// Testing for All Words
				Collection<String> actualAll = thisInsAll.findNearWords("trump", 1);
				assertThat(actualAll, hasItems(expected1, expected2, expected3));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Skipping Tests owing to absence of Model File");
		}
	}

	@Test
	public void testFindNearWordsBigram() {

		NLPSequenceSearcher thisInsBigram = null;

		File f = new File(modelFile);
		if (f.exists() && !f.isDirectory()) {
			try {
				thisInsBigram = NLPSequenceSearcher.getInstance(modelFile);
			} catch (Exception e2) {
				System.out.println("Error....." + e2.getLocalizedMessage());
			}
			String expected4 = "hush_money";
			String expected5 = "much_person";
			String expected6 = "michael_cohen";
			try {
				// Testing for Only Bigrams
				Collection<String> actualBigram = thisInsBigram.findNearWords("trump", 2);
				assertThat(actualBigram, hasItems(expected4, expected5, expected6));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Skipping Tests owing to absence of Model File");
		}
	}

	@Test
	public void testFindNearWordsTrigram() {
		NLPSequenceSearcher thisInsTrigram = null;

		File f = new File(modelFile);
		if (f.exists() && !f.isDirectory()) {
			try {
				thisInsTrigram = NLPSequenceSearcher.getInstance("./src/test/resources/test.model");
			} catch (Exception e2) {
				System.out.println("Error....." + e2.getLocalizedMessage());
			}
			String expected7 = "president_donald_trump";
			String expected8 = "build_border_wall";
			String expected9 = "immunity_federal_investigation";
			try {
				// Testing for Only Trigrams
				Collection<String> actualTrigram = thisInsTrigram.findNearWords("trump", 3);
				assertThat(actualTrigram, hasItems(expected7, expected8, expected9));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Skipping Tests owing to absence of Model File");
		}

	}
}
