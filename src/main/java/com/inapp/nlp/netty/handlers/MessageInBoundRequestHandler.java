package com.inapp.nlp.netty.handlers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inapp.nlp.netty.model.RequestData;
import com.inapp.nlp.netty.model.ResponseData;
import com.inapp.nlp.process.NLPSequenceSearcher;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
public class MessageInBoundRequestHandler extends SimpleChannelInboundHandler<RequestData> {
	
	private static Logger log = LoggerFactory.getLogger(MessageInBoundRequestHandler.class);
	
	private void removeVerb(Collection<String> sWords, MaxentTagger maxentTagger) {
		List<String> verbList = new ArrayList<String>();
		verbList.add("VB");
		verbList.add("VBD");
		verbList.add("VBG");
		verbList.add("VBN");
		verbList.add("VBP");
		verbList.add("VBZ");
		for (Iterator<String> iterator = sWords.iterator(); iterator.hasNext();) {
			String strWord = iterator.next();
			String tag = maxentTagger.tagString(strWord.replace("_", " "));
			String[] eachTag = tag.split("\\s+");
			if (verbList.contains(eachTag[0].split("_")[1])) {
				System.out.println(eachTag[0].split("_")[0] + " Removed");
				log.info("{} removed from Output as it a Verb", strWord);
				iterator.remove();
				continue;
			}
		}
	}
	private void removeAdjective(Collection<String> sWords, MaxentTagger maxentTagger) {
		List<String> adjectiveList = new ArrayList<String>();
		adjectiveList.add("JJ");
		adjectiveList.add("JJR");
		adjectiveList.add("JJS");
		for (Iterator<String> iterator = sWords.iterator(); iterator.hasNext();) {
			String strWord = iterator.next();
			String tag = maxentTagger.tagString(strWord.replace("_", " "));
			String[] eachTag = tag.split("\\s+");
			if (adjectiveList.contains(eachTag[0].split("_")[1])) {
				System.out.println(eachTag[0].split("_")[0] + " Removed");
				log.info("{} removed from Output as it a Adjective", strWord);
				iterator.remove();
				continue;
			}
		}
	}
	private void removeAdverb(Collection<String> sWords, MaxentTagger maxentTagger) {
		List<String> adverbList = new ArrayList<String>();
		adverbList.add("RB");
		adverbList.add("RBR");
		adverbList.add("RBS");
		adverbList.add("WRB");
		for (Iterator<String> iterator = sWords.iterator(); iterator.hasNext();) {
			String strWord = iterator.next();
			String tag = maxentTagger.tagString(strWord.replace("_", " "));
			String[] eachTag = tag.split("\\s+");
			if (adverbList.contains(eachTag[0].split("_")[1])) {
				System.out.println(eachTag[0].split("_")[0] + " Removed");
				log.info("{} removed from Output as it a Adverb", strWord);
				iterator.remove();
				continue;
			}
		}
	}
	private void removePreposition(Collection<String> sWords, MaxentTagger maxentTagger) {
		List<String> adverbList = new ArrayList<String>();
		adverbList.add("IN");
		for (Iterator<String> iterator = sWords.iterator(); iterator.hasNext();) {
			String strWord = iterator.next();
			String tag = maxentTagger.tagString(strWord.replace("_", " "));
			String[] eachTag = tag.split("\\s+");
			if (adverbList.contains(eachTag[0].split("_")[1])) {
				System.out.println(eachTag[0].split("_")[0] + " Removed");
				log.info("{} removed from Output as it a Preposition", strWord);
				iterator.remove();
				continue;
			}
		}
	}
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, RequestData msg) throws Exception {
		RequestData requestData = (RequestData) msg;
		log.info("Channel Read complete...." + requestData);
		String sQuery = requestData.getRequestQuery();
		String sReturn = "";
		log.debug("Request received... Query = " + sQuery);
		if (!sQuery.trim().equalsIgnoreCase("Hi")) {
			Collection<String> sWords = null;
			try {
				sWords = NLPSequenceSearcher.getInstance("").findNearWords(sQuery.replaceAll("'", ""),
						requestData.getMinGram());
				log.debug("Mingram Value Send is " + requestData.getMinGram());
				log.debug("posFilter Value Send is " + requestData.getposFilter());
				int posValue = requestData.getposFilter();
		        MaxentTagger maxentTagger = new MaxentTagger("english-bidirectional-distsim.tagger");
				for (Iterator<String> iterator = sWords.iterator(); iterator.hasNext();) {
					String strWord = iterator.next();
					String[] strWordLength = strWord.split("\\_");
					long counter = strWord.split("\\_", -1).length - 1;
					if ((counter > 2 || counter == 0) || 
							(strWordLength[0].length() < 3) || 
							(strWord.contains("amp")) ) {
						log.info("Removing Word {} from Output Stream Conditions Count: {}  Length: {}", 
								strWord, counter, strWordLength[0].length());
						iterator.remove();
					}
					
				}
				removePreposition(sWords, maxentTagger);
				if (posValue != 0) {
					if (posValue == 1) {
						removeVerb(sWords, maxentTagger);
					} else if (posValue == 2) {
						removeAdjective(sWords, maxentTagger);
					} else if (posValue == 3) {
						removeAdverb(sWords, maxentTagger);
					} else if (posValue == 4) {
						removeVerb(sWords, maxentTagger);
						removeAdjective(sWords, maxentTagger);
					} else if (posValue == 5) {
						removeAdjective(sWords, maxentTagger);
						removeAdverb(sWords, maxentTagger);
					} else if (posValue == 6) {
						removeVerb(sWords, maxentTagger);
						removeAdverb(sWords, maxentTagger);
					} else if (posValue == 7) {
						removeVerb(sWords, maxentTagger);
						removeAdjective(sWords, maxentTagger);
						removeAdverb(sWords, maxentTagger);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			sReturn = sWords.toString();
			log.info("Words = " + sReturn);
		} else
			sReturn = "Hello";
		ResponseData responseData = new ResponseData();
		responseData.setWords(sReturn);
		ctx.write(responseData);
		log.debug("Sending response ... Data=" + responseData);
		ctx.flush();
	}
}