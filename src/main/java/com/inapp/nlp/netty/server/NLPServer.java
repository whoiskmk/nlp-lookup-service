package com.inapp.nlp.netty.server;

import org.apache.commons.cli.CommandLine;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inapp.nlp.netty.handlers.MessageInBoundRequestHandler;
import com.inapp.nlp.netty.helpers.RequestDecoder;
import com.inapp.nlp.netty.helpers.ResponseDataEncoder;
import com.inapp.nlp.process.NLPSequenceSearcher;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class NLPServer {
	private int port;
	private String hostName;

	private NLPServer(int port, String hostName) {
		this.port = port;
		this.hostName = hostName;
	}
	private static Logger log = LoggerFactory.getLogger(NLPServer.class);

	public static void main(String[] args) throws Exception {
		int port = 9191;
		String hostName = "127.0.0.1";
		String modelFilePath = "";

		CommandLine commandLine;
		Option option_i = Option.builder("i")
				.required(true)
				.desc("Name and Location of the NLP Model File")
				.longOpt("input")
				.hasArg()
				.build();
		
		Option option_p = Option.builder("p")
				.required(false)
				.desc("Port Number Of Server")
				.longOpt("port")
				.hasArg()
				.build();
		
		Option option_h = Option.builder("h")
				.required(false)
				.desc("Host Name of the Server")
				.longOpt("host")
				.hasArg()
				.build();
		
		Options options = new Options();
		CommandLineParser parser = new DefaultParser();
		options.addOption(option_i);
		options.addOption(option_p);
		options.addOption(option_h);

		try {
			commandLine = parser.parse(options, args);

			if (commandLine.hasOption("i")) {
				String tmp = commandLine.getOptionValue("input");
				modelFilePath = tmp;
			}
			if (commandLine.hasOption("p")) {
				String tmp = commandLine.getOptionValue("port");
				port = Integer.parseInt(tmp);
			}
			if (commandLine.hasOption("h")) {
				String tmp = commandLine.getOptionValue("host");
				hostName = tmp;
			}
		} catch (Exception e) {
			log.error("Error....."+e.getLocalizedMessage());
		}
		// Load the NLP singleton on startup...
		log.info("Loading NLP Search engine ...");
		NLPSequenceSearcher.getInstance(modelFilePath);
		log.info("Starting the server ...");
		new NLPServer(port, hostName).run();
	}

	private void run() throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.handler(new LoggingHandler(LogLevel.DEBUG))
				.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							/**
							 * Define inbound and outbound handlers that will process requests and output in
							 * the correct order.
							 **/
							ch.pipeline().addLast(new RequestDecoder(), 
									new ResponseDataEncoder(),
									new MessageInBoundRequestHandler());
						}
					})
					.childOption(ChannelOption.SO_KEEPALIVE, true);

			ChannelFuture f = b.bind(hostName, port).sync();
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
