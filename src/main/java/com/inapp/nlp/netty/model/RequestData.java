package com.inapp.nlp.netty.model;
import java.nio.charset.Charset;
public class RequestData {
	private final Charset charset = Charset.forName("UTF-8");
	private String requestQuery;
	private int minGram = 1;
	private int posFilter = 0;
	
	public RequestData(String requestQuery, int minGram,int posFilter) {
		this.requestQuery = requestQuery;
		this.minGram = minGram;
		this.posFilter = posFilter;
	}
	
	public String getRequestQuery() {
		return requestQuery;
	}
	public void setRequestQuery(String requestQuery) {
		this.requestQuery = requestQuery;
	}
	public byte[] getDataBytes() {
		return this.requestQuery.getBytes(charset);
	}
	
	public int getLength() {
		return this.requestQuery.length();
	}
	
	@Override
    public String toString() {
        return "RequestData [" 
        		+ "Words =" + this.requestQuery 
        		+ " MinGram =" + this.minGram 
        		+ " posFilter =" + this.posFilter
        		+ "]";
    }
	public int getMinGram() {
		return minGram;
	}
	public void setMinGram(int minGram) {
		this.minGram = minGram;
	}
	public int getposFilter() {
		return posFilter;
	}
	public void setposFilter(int posFilter) {
		this.posFilter = posFilter;
	}
	
}