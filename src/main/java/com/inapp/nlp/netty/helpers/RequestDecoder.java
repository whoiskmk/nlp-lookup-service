package com.inapp.nlp.netty.helpers;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.inapp.nlp.netty.model.RequestData;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

/**
 * The biggest difference between ReplayingDecoder and ByteToMessageDecoder is
 * that ReplayingDecoder allows you to implement the decode() and decodeLast()
 * methods just like all required bytes were received already, rather than
 * checking the availability of the required bytes.
 * 
 * @author InApp
 *
 */
public class RequestDecoder extends ReplayingDecoder<RequestData> {
	private static Logger log = LoggerFactory.getLogger(RequestDecoder.class);

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Object> out) throws Exception {
		if (!buf.isReadable()) {
			log.error("No readable bytes for request decoder");
			return;
		}
		byte[] bytes;
		int length = buf.readInt();
		log.info("Request buffer length " + length);
		if (length > 1000) {
			log.error("Cannot process requests with characters length = " + length);
			return;
		}
		// Read unigram/bigram/trigram field
		int gramVal = buf.readInt();
		int posFilter = buf.readInt();
		log.info("gramval is "+gramVal);
		log.info("posFilter is "+posFilter);
		// read the string
		bytes = new byte[length];
		buf.getBytes(buf.readerIndex(), bytes);
		String query = new String(bytes);
		log.debug("Request decoder ... Query = " + query);
		RequestData data = new RequestData(query, gramVal,posFilter);
		out.add(data);
	}
}