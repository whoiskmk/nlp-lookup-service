package com.inapp.nlp.netty.helpers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inapp.nlp.netty.model.ResponseData;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

public class ResponseDataDecoder extends ReplayingDecoder<ResponseData> {
	private static Logger log = LoggerFactory.getLogger(ResponseDataDecoder.class);

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

		if (!in.isReadable())
			return;

		byte[] bytes;
		int length = in.readInt();
		if (length > 25)
			return;
		log.info("Response buffer length " + length);
		if (in.hasArray()) {
			bytes = in.array();
		} else {
			bytes = new byte[length];
			in.getBytes(in.readerIndex(), bytes);
		}

		String response = new String(bytes);
		log.info("ResponseDataDecoder ..." + response);
		ResponseData data = new ResponseData();
		
		System.out.println("Data is*********** "+data.toString());
		
		data.setWords(response);
		out.add(data);
	}
}