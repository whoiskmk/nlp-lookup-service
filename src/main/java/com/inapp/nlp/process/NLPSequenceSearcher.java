package com.inapp.nlp.process;

import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.sequencevectors.SequenceVectors;
import org.deeplearning4j.models.sequencevectors.interfaces.SequenceElementFactory;
import org.deeplearning4j.models.sequencevectors.serialization.VocabWordFactory;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;

public class NLPSequenceSearcher {
	private static Logger log = LoggerFactory.getLogger(NLPSequenceSearcher.class);
	private static NLPSequenceSearcher instance;
	private InputStream inputStream = null;
	static String path = "";
	private SequenceVectors<VocabWord> vectors = null;

	private NLPSequenceSearcher(String modelFilename) throws Exception {

		/*
		 * Load the model and test with some data
		 */
		log.info("Loading the NLP Model ...");
		path = modelFilename;
		log.info("Reading sequence vectors from the NLP Model ...");
		SequenceElementFactory<VocabWord> f1 = new VocabWordFactory();
		InputStream modelInputStream = new FileInputStream(path);

		vectors = WordVectorSerializer.readSequenceVectors(f1, modelInputStream);
		if (vectors == null) {
			log.error("NLPSequenceSearcher reading sequence vector from model failed.");
			throw new Exception("NLPSequenceSearcher reading sequence vector from model failed.");
		} else {
			log.info("Model Loading Successful");
		}
		log.info("NLP Model is loaded into memory ...");
	}

	@Override
	public void finalize() {
		log.info("NLPSequenceSearcher is getting destroyed ...");
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				log.error("Error....." + e.getLocalizedMessage());
			}
		}
	}

	public static synchronized NLPSequenceSearcher getInstance(String modelFileName) throws Exception {
		if (instance == null) {
			log.info("NLPSequenceSearcher creating instance ...");
			instance = new NLPSequenceSearcher(modelFileName);
		}
		return instance;
	}

	/**
	 * function to check word similarity
	 * 
	 * @param searchWord
	 * @return
	 * @throws Exception
	 */
	public Collection<String> findNearWords(String searchWord, int minGram) throws Exception {
		
		Collection<String> result = new ArrayList<String>();
		try {
			log.info("NLPSequenceSearcher finding sequence vector for word [" + searchWord + "].");
			if (vectors == null) {
				log.error("Loading NLP model is UnSuccesfull !!!");
				return null;
			}
			if (searchWord != null && searchWord.trim().length() > 0) {
				searchWord = searchWord.trim().replaceAll(" ", "_");
				log.info("Finding Words near to " + searchWord);
				long stime = Calendar.getInstance().getTimeInMillis();
				// Prints out the closest 100 words
				Collection<String> lst = new ArrayList<String>();
				try {
					lst = vectors.wordsNearest(searchWord, 1000);
				} catch (Exception e) {
					log.error("Could not find near by words. Error = " + e.getLocalizedMessage());
					log.error("Error stack", e);
				}
				long etime = Calendar.getInstance().getTimeInMillis();
				log.info("Look up Time is : " + (etime - stime) + " MilliSeconds");
				if (lst != null && lst.size() > 0) {
					if (minGram == 2) {
						for (String word : lst) {
							int count = CharMatcher.is('_').countIn(word);
							if (count == 1) {
								if (word.indexOf('_') == 0 || word.indexOf('_') == word.length())
									continue;
								else {
									result.add(word);
								}
							}
						}
					} else if (minGram == 3) {
						for (String word : lst) {
							int count = CharMatcher.is('_').countIn(word);
							if (count == 2) {
								if (word.indexOf('_') == 0 || word.indexOf('_') == word.length())
									continue;
								else {
									result.add(word);
								}
							}
						}
					} else {
						result = lst;
					}
					log.info("100 Words closest to '" + searchWord + "': " + Arrays.toString(lst.toArray()));
				} else {
					log.info("No Words matched in Corpus !!!!");
				}

			}
		} catch (Exception e) {
			log.error("Error in finding near by words. Error = " + e.getLocalizedMessage());
			log.error("Error stack", e);
			throw e;
		}
		return result;
	}

	public static void main(String[] args) throws Exception {
		log.info("Testing NLPSequenceSearcher ....");
		try {
			NLPSequenceSearcher thisIns = NLPSequenceSearcher.getInstance("");
			thisIns.findNearWords("trump", 1);
		} catch (Exception e) {
			log.info("Error ---------" + e.getMessage());
			e.printStackTrace();
		}
	}
}