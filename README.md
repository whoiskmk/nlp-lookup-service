# Elasticsearch-PQL Server

The NLP TCP Server that serves the ES Plugin by creating the Sequence Vectors needed for the NLP Model


# Dependencies Needed

The below dependencies are needed for the Server. They are already added to the classpath using the pom.xml File


	<groupId>io.netty</groupId>
	<artifactId>netty-all</artifactId>
	<version>4.1.29.Final</version>

	<groupId>org.slf4j</groupId>
	<artifactId>slf4j-api</artifactId>
	<version>1.7.2</version>

	<groupId>ch.qos.logback</groupId>
	<artifactId>logback-classic</artifactId>
	<version>1.0.9</version>

	<groupId>ch.qos.logback</groupId>
	<artifactId>logback-core</artifactId>
	<version>1.0.9</version>

	<groupId>org.deeplearning4j</groupId>
	<artifactId>deeplearning4j-nlp</artifactId>
	<version>1.0.0-beta2</version>

	<groupId>org.nd4j</groupId>
	<artifactId>nd4j-native-platform</artifactId>
	<version>1.0.0-beta2</version>

# Creating the Server Application

The Server Application can be created by running the Commands:

    mvn clean
    mvn package

This will create a Zip file in the location /target/releases/

# Running the Server Application

This Server has to be running while executing the ElasticSearch Queries

Run the Server by the Commands:

    unzip target/releases/nlp-server-0.0.1-SNAPSHOT.zip -d ${LOCATION_TO_EXTRACT_TO}
    java -jar Extract/nlp-server-0.0.1-SNAPSHOT.jar -i ${PATH_TO_MODEL_FILE} -p ${PORT_NUMBER} -h ${HOST_NAME}

|  Parameter Variable  | 			Parameter Description  		                |   Necessity  |   Type Of Value    |
| -------------------- | ------------------------------------------------------ | ------------ | ------------------ |
|         -i           |            Input Parameter File name                   |   Required   |    Path(String)    |
|         -h           |            Host Name                                   |   Optional   |    Path(String)    |
|         -p           |            Port Number                                 |   Optional   |    Path(String)    |

The default values for Port Number and Host Name is 9191 and localhost respectively.

# Sample Execution

Create the Server Jar using the Command:

	mvn clean
	mvn package
	
Unzip the zip in the target/releases Folder using the Commands:

	unzip target/releases/nlp-server-0.0.1-SNAPSHOT.zip -d Extract
	
Run the Server by running the command:

	java -jar Extract/nlp-server-0.0.1-SNAPSHOT.jar -i {Path_To_Model_File} -p 9191 -h 127.0.0.1
	
The Server Will be Running on Port Number 9191 with HostName "127.0.0.1"

# Caveats

* The Server has to be running while executing the ES queries
* If no value is provided for the Port Number or Host Name External parameter , The default value will be 9191 and "localhost" respectively.
* For Unit Test to work, Add the created model file in the [src/test/resources](./src/test/resources) Folder .